#include <ulib.h>
#include <stdio.h>

double pi;

void PI()
{
	long long num_steps = 2000000;
	double step;

	double x, sum=0.0;
	int i;
	step = 1./(double)num_steps;

	for (i=0; i<num_steps; i++)
	{
		x = (i + .5)*step;
		sum += 4.0/(1.+ x*x);
	}
	pi = sum*step;
	return;
}

int main(void) {    
    int n,pid;
	sem_t mutex = sem_init(1);
	if ((pid = fork()) != 0) {
		sem_wait(mutex);
		cprintf("test1:process %d priority level %d start\n",pid,nice(pid));
		sem_post(mutex);		
		exit(0);
	}
    
    for (n = 20; n >= 0; n--) {
        if ((pid = nice_fork(n*7)) != 0) {
			sem_wait(mutex);			
			cprintf("test2:process %d priority level %d start\n",pid,nice(pid));
			sem_post(mutex);			
			PI();
			sem_wait(mutex);	
			cprintf("test2:process %d priority level %d exit,time : %d\n",pid,nice(pid),pid_time(pid));
			sem_post(mutex);	
			exit(0);
        }
    }
    
    for (n = 0; n <= 20; n++) {
        if ((pid = nice_fork(n*7)) != 0) {
			sem_wait(mutex);						
			cprintf("test3:process %d priority level %d start\n",pid,nice(pid));
			sem_post(mutex);						
			PI();
			sem_wait(mutex);						
			cprintf("test3:process %d priority level %d exit,time : %d\n",pid,nice(pid),pid_time(pid));
			sem_post(mutex);						
			exit(0);
        }
    }
        
    return 0;
}
