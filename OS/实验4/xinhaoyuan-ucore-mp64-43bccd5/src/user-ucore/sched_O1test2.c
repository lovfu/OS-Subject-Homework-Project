#include <ulib.h>
#include <stdio.h>

double pi;

void PI()
{
	long long num_steps = 10000;
	double step;

	double x, sum=0.0;
	int i;
	step = 1./(double)num_steps;


	for (i=0; i<num_steps; i++)
	{
		x = (i + .5)*step;
		sum += 4.0/(1.+ x*x);
	}
	pi = sum*step;
	return;
}

int main(void) {    
    int i,pid;
    for(i = 0;i < 10;i++)
    {
		if ((pid = fork()) != 0) {
			cprintf("proc:%d start\n",nice(pid));
			PI();
			exit(0);
		}
	}
    return 0;
}
