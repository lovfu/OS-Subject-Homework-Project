#ifndef __KERN_SCHEDULE_SCHED_O1_H__
#define __KERN_SCHEDULE_SCHED_O1_H__

#include <sched.h>

extern struct sched_class O1_sched_class;

#endif /* !__KERN_SCHEDULE_SCHED_O1_H__ */
